let selectRow = null;
// Show Alerts
function showAlert(message, className) {
  const div = document.createElement("div");
  div.className = `alert alert-${className}`;
  div.appendChild(document.createTextNode(message));
  const container = document.querySelector(".container");
  const main = document.querySelector(".main");
  container.insertBefore(div, main);
  setTimeout(() => document.querySelector(".alert").remove(), 3000);
}
// Clear All Fields
function clearFields() {
  document.querySelector("#firstName").value = "";
  document.querySelector("#lastName").value = "";
  document.querySelector("#roolNo").value = "";
}
// Add Data
document.querySelector("#student-form").addEventListener("submit", (e) => {
  e.preventDefault();

  // Get Form Values
  const firstName = document.querySelector("#firstName").value;
  const lastName = document.querySelector("#lastName").value;
  const roolNo = document.querySelector("#roolNo").value;

  //   validate
  if (firstName == "" || lastName == "" || roolNo == "") {
    showAlert("Please fill in all fields", "danger");
  } else {
    if (selectRow == null) {
      const list = document.querySelector("#student-list");
      const row = document.createElement("tr");
      row.innerHTML = `
        <td>${firstName}</td>
        <td>${lastName}</td>
        <td>${roolNo}</td>
        <td>
        <a href="#" class="btn btn-warning btn-sm edit">Edit</a>
        <a href="#" class="btn btn-danger btn-sm delete">Delete</a>
        </td>
        `;
      list.appendChild(row);
      selectRow = null;
      showAlert("Student Added", "success");
    } else {
      selectRow.children[0].textContent = firstName;
      selectRow.children[1].textContent = lastName;
      selectRow.children[2].textContent = roolNo;
      selectRow = null;
      showAlert("Student Info Edited", "info");
    }
    clearFields();
  }
});

// Edit and Delete Data

document.querySelector("#student-list").addEventListener("click", (e) => {
  target = e.target;
  if (target.classList.contains("edit")) {
    selectRow = target.parentElement.parentElement;
    document.querySelector("#firstName").value =
      selectRow.children[0].textContent;
    document.querySelector("#lastName").value =
      selectRow.children[1].textContent;
    document.querySelector("#roolNo").value = selectRow.children[2].textContent;
  } else {
    target.parentElement.parentElement.remove();
    showAlert("Student Data Deleted", "danger");
  }
});
